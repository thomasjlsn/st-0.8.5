static const char *colorname[] = {
	/* 8 normal colors */
	"#073642",
	"#DB322F",
	"#849801",
	"#B48801",
	"#268AD1",
	"#D23681",
	"#2AA097",
	"#EDE7D4",

	/* 8 bright colors */
	"#012B36",
	"#CA4B16",
	"#586E74",
	"#657B82",
	"#829395",
	"#6C71C3",
	"#92A0A0",
	"#FCF5E2",
};

/*
 * Default colors (colorname index)
 * foreground, background, cursor, reverse cursor
 */

unsigned int defaultfg = 0;
unsigned int defaultbg = 15;
unsigned int defaultcs = 0;
static unsigned int defaultrcs = 7;
