// Adapted from: https://github.com/jnurmine/Zenburn

static const char *colorname[] = {
	/* 8 normal colors */
	"#1F1F1F",
	"#CC9393",
	"#5F7F5F",
	"#FFD7A7",
	"#8CB0D3",
	"#8F8F8F",
	"#71D3B4",
	"#DFE4CF",

	/* 8 bright colors */
	"#6F6F6F",
	"#ECB3B3",
	"#FFD7A7",
	"#8CB0D3",
	"#8F8F8F",
	"#71D3B4",
	"#DFE4CF",
	"#FFCFAF",
};

/*
 * Default colors (colorname index)
 * foreground, background, cursor, reverse cursor
 */

unsigned int defaultfg = 7;
unsigned int defaultbg = 0;
unsigned int defaultcs = 163;
static unsigned int defaultrcs = 232;
