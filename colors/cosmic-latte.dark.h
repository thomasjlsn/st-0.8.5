// Adapted from: https://github.com/haystackandroid/cosmic_latte

static const char *colorname[] = {
	/* 8 normal colors */
	"#202A31",
	"#C17B8D",
	"#7D9761",
	"#B28761",
	"#5496BD",
	"#9B85BB",
	"#459D90",
	"#ABB0C0",

	/* 8 bright colors */
	"#898F9E",
	"#C17B8D",
	"#7D9761",
	"#B28761",
	"#5496BD",
	"#9B85BB",
	"#459D90",
	"#C5CBDB",
};

/*
 * Default colors (colorname index)
 * foreground, background, cursor, reverse cursor
 */

unsigned int defaultfg = 7;
unsigned int defaultbg = 0;
unsigned int defaultcs = 7;
static unsigned int defaultrcs = 257;
