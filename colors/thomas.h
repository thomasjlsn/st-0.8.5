/*      __  __
 *     / /_/ /_  ____  ____ ___  ____ ______
 *    / __/ __ \/ __ \/ __ `__ \/ __ `/ ___/
 *   / /_/ / / / /_/ / / / / / / /_/ (__  )
 *   \__/_/ /_/\____/_/ /_/ /_/\__,_/____/
 *
 *        Thomas' default colorscheme
 *   (A Varient of the tomorrow colorscheme)
 *
 *********************************************/

static const char *colorname[] = {
	/* 8 normal colors */
	"#000000",
	"#CC6666",
	"#B5BD68",
	"#F0C674",
	"#81A2BE",
	"#B294BB",
	"#8ABEB7",
	"#C5C8C6",

	/* 8 bright colors */
	"#000000",
	"#D54E53",
	"#B9CA4A",
	"#E7C547",
	"#7AA6DA",
	"#C397D8",
	"#70C0B1",
	"#EAEAEA",
};

/*
 * Default colors (colorname index)
 * foreground, background, cursor, reverse cursor
 */

unsigned int defaultfg = 7;
unsigned int defaultbg = 0;
unsigned int defaultcs = 163;
static unsigned int defaultrcs = 257;
