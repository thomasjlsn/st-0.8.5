static const char *colorname[] = {
	/* 8 normal colors */
	"#000000",
	"#D54E53",
	"#B9CA4A",
	"#E6C547",
	"#7AA6DA",
	"#C397D8",
	"#70C0BA",
	"#EAEAEA",

	/* 8 bright colors */
	"#585858",
	"#FF3334",
	"#9EC400",
	"#E7C547",
	"#7AA6DA",
	"#B77EE0",
	"#54CED6",
	"#FFFFFF",
};

/*
 * Default colors (colorname index)
 * foreground, background, cursor, reverse cursor
 */

unsigned int defaultfg = 7;
unsigned int defaultbg = 0;
unsigned int defaultcs = 256;
static unsigned int defaultrcs = 257;
