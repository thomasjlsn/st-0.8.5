// Adapted from: https://github.com/haystackandroid/cosmic_latte

static const char *colorname[] = {
	/* 8 normal colors */
	"#fff8e7",
	"#c44756",
	"#1f8332",
	"#916d03",
	"#0075c9",
	"#a154ae",
	"#007f8a",
	"#485a62",

	/* 8 bright colors */
	"#63757e",
	"#c44756",
	"#1f8332",
	"#916d03",
	"#0075c9",
	"#a154ae",
	"#007f8a",
	"#364850"
};

/*
 * Default colors (colorname index)
 * foreground, background, cursor, reverse cursor
 */

unsigned int defaultfg = 7;
unsigned int defaultbg = 0;
unsigned int defaultcs = 7;
static unsigned int defaultrcs = 257;
