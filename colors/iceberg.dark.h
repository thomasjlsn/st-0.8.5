/*      _________________________________________
 *      \_ _/ ____| ____| ___ \ ____| ___ \  ___/
 *       | | |____| ____| ___ < ____| __  / |__ \
 *      /___\_____|_____|_____/_____|_| \_\_____/
 *
 *  Adapted from: https://github.com/cocopon/iceberg.vim
 *
 *******************************************************/

static const char *colorname[] = {
	/* 8 normal colors */
	"#1E2132",
	"#E27878",
	"#B4BE82",
	"#E2A478",
	"#84A0C6",
	"#A093C7",
	"#89B8C2",
	"#C6C8D1",

	/* 8 bright colors */
	"#6B7089",
	"#E98989",
	"#C0CA8E",
	"#E9B189",
	"#91ACD1",
	"#ADA0D3",
	"#95C4CE",
	"#D2D4DE",
};

/*
 * Default colors (colorname index)
 * foreground, background, cursor, reverse cursor
 */

unsigned int defaultfg = 7;
unsigned int defaultbg = 0;
unsigned int defaultcs = 7;
static unsigned int defaultrcs = 257;
