/*      _________________________________________
 *      \_ _/ ____| ____| ___ \ ____| ___ \  ___/
 *       | | |____| ____| ___ < ____| __  / |__ \
 *      /___\_____|_____|_____/_____|_| \_\_____/
 *
 *  Adapted from: https://github.com/cocopon/iceberg.vim
 *
 *******************************************************/

static const char *colorname[] = {
	/* 8 normal colors */
	"#DCDFE7",
	"#CC517A",
	"#668E3D",
	"#C57339",
	"#2D539E",
	"#7759B4",
	"#3F83A6",
	"#33374C",

	/* 8 bright colors */
	"#8389A3",
	"#CC3768",
	"#598030",
	"#B6662D",
	"#22478E",
	"#6845AD",
	"#327698",
	"#262A3F",
};

/*
 * Default colors (colorname index)
 * foreground, background, cursor, reverse cursor
 */

unsigned int defaultfg = 7;
unsigned int defaultbg = 0;
unsigned int defaultcs = 256;
static unsigned int defaultrcs = 257;
